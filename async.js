const fetch = require('node-fetch');

const fetchGithubUser = async (username) => {
  const url = `https://api.github.com/users/${username}`;
  const res = await fetch(url);
  const user = await res.json();

  if(res.status!==200)
    throw Error(user.message);

  return user;
}

const showGithubUser = async (username) => {
  try {
    const user = await fetchGithubUser(username);
    console.log(user);
  } catch (error) { 
    console.log( `Error: ${error.message}`);
  }
}

showGithubUser('nvs2');


/**
 *Use Class
 */
// class GithubApiClient {
//   async fetchUser(username){
//     const url = `https://api.github.com/users/${username}`;
//     const res = await fetch(url);
//     const user = await res.json();

//     return user;
//   }
// }